// Display/Hide WYMeditor
function wymToggleDisplay()
{
	txt = document.getElementById("edit-body");
	txt.style.display = (txt.style.display == "") ? "none" : "";
	wymdiv = document.getElementById("wym-div");
	wymdiv.style.display = (wymdiv.style.display == "") ? "none" : "";
	
	// Populate either the editor or the textarea, according to what is to be shown.
	if(wymdiv.style.display == ""){
	  // When hiding the iframe, Gecko forgets the 'styleWithCSS' setting.
    if (moz) setTimeout("execCom('styleWithCSS',false,false);",500);
    setHTML();
	} else
		getCleanHTML();
		
}