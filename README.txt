
*******************************************************
This module is under development. Use at your own risk.
*******************************************************

Description
-----------
This module integrates WYMeditor 0.2 in Drupal.
WYMeditor is an XHTML/Javascript WYSIWYM editor.

Note: a patched version of WYMeditor 0.2 is included in the module archive.
Current version is 0.2.2 Stable (26/01/2007)

Resources
---------
WYMeditor homepage : http://www.wymeditor.org
WYMeditor demo : http://www.wymeditor.org/en/demo/

Prerequisites
-------------
This module currently works with Drupal 4.7

Installation/Configuration
--------------------------
Please read INSTALL.txt

Known limitations
-----------------
- WYMeditor can be set to one textarea per page (currently 'edit-body')
